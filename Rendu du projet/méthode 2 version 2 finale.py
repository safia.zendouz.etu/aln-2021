# -*- coding: utf-8 -*-
"""
Created on Sun Jun  6 02:19:21 2021

@author: Safia
"""


from math import sqrt
import numpy as np
import scipy.linalg as nla



A = np.array ([
    [3, 97.8, 119, 2.6, 7.4, 0.2, 1.8, 2.2, 53, 34, 28.3, 21],
    [0.2, 80.5, 121, 2.1, 5.3, 0.5, 1.4, 1.4, 47.4, 20.5, 27.8, 19.9],
    [0.4, 6.1, 67, 4.2, 9.9, 3, 1.5, 2.5, 10.6, 21, 15.1, 23.1],
    [7.3, 106.4, 129, 1.9, 14.7, 1.1, 2.1, 3.7, 45.9, 12.5, 22.1, 29.9],
    [4.6, 170.6, 79, 1, 26.4, -4.4, 1.4, 11.8, 27.6, 23, 26, 31],
    [6.7, 69.3, 98, 2.4, 26.2, -1.4, 1.4, 4.9, 32.7, 35, 22.7, 27],
    [3.7, 86, 108, 2.2, 10.6, 0.1, 2, 2, 45.4, 34, 30.8, 19.3],
    [2.1, 120.7, 100, 3.3, 11.7, -1, 1.4, 4.6, 35.6, 33, 27.8, 24.5],
    [4.5, 71.1, 94, 3.1, 14.7, -3.5, 1.4, 12, 20.7, 10, 18.4, 23.5],
    [0.9, 18.3, 271, 2.9, 5.3, 0.5, 1.5, 2.3, 50.7, 22, 20.1, 16.8], 
    [2.9, 70.9, 85, 3.2, 7, 1.5, 1.5, 4, 25, 35, 18.9, 21.4],
    [3.6, 65.5, 131, 2.8, 6, -0.6, 1.8, 1.6, 55.7, 34, 28.4, 15.7],
    [2.5, 72.4, 129, 2.6, 4.9, 0.7, 1.4, 1.7, 45.6, 34, 28.2, 16.9],
    [4.9, 108.1, 77, 2.8, 17.6, -1.9, 1.4, 6, 14.8, 25, 24.3, 24.4],
    [5.1, 46.9, 84, 2.8, 10.2, -2, 1.6, 4.9, 18.5, 20, 21.5, 19.3],
    [3.3, 43.3, 73, 3.7, 14.9, 1.1, 1.5, 2.1, 10.1, 19, 16, 20.6],
    [1.5, 49, 114, 3.2, 7.9, 0.3, 1.8, 1.6, 46.8, 26, 26.3, 17.9]],
    dtype=np.float64)


m , n = A.shape



def centrerDonnees(A):
    vect = np.mean(A,0)
    for i in range (m):
        for j in range (n):
            A[i,j]=A[i,j]-vect[j]
    return A

centrerDonnees(A)

def adimensionner(A):
    
    for j in range (n):
        A[:,j]=A[:,j]/np.std(A[:,j])
        
        
    return A

adimensionner(A)



def reflecteur (p, v) :
    n = v.shape[0]
    F = np.eye(n)  - 2 * np.outer (v,v)
    Q = np.eye (p, dtype=np.float64)
    Q [p-n:p,p-n:p] = F
    return Q

B = np.copy(A)
m , n = B.shape

x = B[0:m,0]
vect = np.copy(x)
vect[0] = vect[0] + np.sign(vect[0])*nla.norm(x,2)
vect = (1/nla.norm(vect,2))*vect
VL = [vect]
Q = reflecteur (m, vect)
B = np.dot(Q,B)
print(B)

VR =[]

for i in range(1,n-1):
  x = B[i-1,i:n]
  vect = np.copy(x)
  vect[0] = vect[0] + np.sign(vect[0])*nla.norm(x,2)
  vect = (1/nla.norm(vect,2))*vect
  VR.append(vect)
  Q = reflecteur (n, vect)
  B = np.dot(B, Q)
  x = B[i:m,i]
  vect = np.copy(x)
  vect[0] = vect[0] + np.sign(vect[0])*nla.norm(x,2)
  vect = (1/nla.norm(vect,2))*vect
  VL.append(vect)
  Q = reflecteur (m, vect)
  B = np.dot (Q, B)

x = B[n-1:m,n-1]
vect = np.copy(x)
vect[0] = vect[0] + np.sign(vect[0])*nla.norm(x,2)
vect = (1/nla.norm(vect,2))*vect
VL.append (vect)
Q = reflecteur (m, vect)
B = np.dot (Q, B)


BD=[]
BD = BD[0:n,0:n]
print(BD)
for k in VL:
  print(k,"\n")
print(len(VL))
len(VR)

H = np.zeros ([2*m, 2*n], dtype=np.float64)
H [0:n,n:2*n] = np.transpose(BD)
H [n:2*n,0:n] = BD

P = np.zeros ([2*n,2*n], dtype=np.float64)
for i in range (0,n) :
    P[i,2*i] = 1
    P[n+i,2*i+1] = 1

T = np.dot (np.transpose(P), np.dot (H, P))
d = np.zeros (2*n, dtype=np.float64)
e = np.array ([ T[i+1,i] for i in range (0,2*n-1) ], dtype=np.float64)
eigvals, eigvecs = nla.eigh_tridiagonal(d, e)
Lambda = eigvals [n:2*n]
Q = eigvecs [:,n:2*n]
Y = np.sqrt(2) * np.dot (P, Q)
newVt = np.transpose (Y[0:n,:])

newU = np.zeros ([m,n], dtype=np.float64)
newU[0:n,:] = Y[n:2*n,:]
newSigma = np.array (np.diag (Lambda), dtype=np.float64)



for i in range (n-1, -1, -1) :
    Q = reflecteur (m, VL[i])
    newU = np.dot (Q, newU)
for i in range (n-3, -1, -1) :
    Q = reflecteur (n, VR[i])
    newVt = np.dot (newVt, Q)
newA = np.dot (newU, np.dot (newSigma, newVt))

VectS = np.transpose(newVt)

Ptris = np.dot(A, np.transpose((VectS[:,0],VectS[:,1])))

print(Ptris)