# QUESTION 1

import numpy as np

def f(x):
    return x**2-4
def df(x):
    return 2*x
u=np.zeros(6)
u[0]=3
for i in range(len(u)-1):
    u[i+1]=u[i]-f(u[i])/df(u[i])
print(u)

# [3.         2.16666667 2.00641026 2.00001024 2.         2.        ]
# Si on commence par  𝑢0<0  on converge vers -2

def f(x):
    return (x-2)**2
def df(x):
    return 2*x
u=np.zeros(40)
u[0]=3
for i in range(len(u)-1):
    u[i+1]=u[i]-f(u[i])/df(u[i])
print(u)


# [3.         2.83333333 2.71078431 2.61759831 2.54473998 2.48643507
#  2.43885308 2.39936894 2.36613196 2.33780459 2.3133988  2.29217055
#  2.27354985 2.25709331 2.24245126 2.22934448 2.21754754 2.20687653
#  2.19718005 2.18833235 2.18022821 2.17277895 2.16590927 2.15955492
#  2.15366071 2.14817897 2.14306836 2.13829283 2.13382083 2.1296246
#  2.12567964 2.12196427 2.1184592  2.11514722 2.11201295 2.10904259
#   2.1062237  2.1035451  2.10099664 2.09856915]

# On remarque que la convergence est moins rapide dans ce cas pour  𝑓(𝑥)=(𝑥−2)2

def f(x):
    return x**2-3
def df(x):
    return 2*x
u=np.zeros(10)
u[0]=3
for i in range(len(u)-1):
    u[i+1]=u[i]-f(u[i])/df(u[i])
print(u)

# [3.         2.         1.75       1.73214286 1.73205081 1.73205081
#  1.73205081 1.73205081 1.73205081 1.73205081]

# On en déduit alors que  3⎯⎯√=1.73205081

# QUESTION 2
import numpy as np

# f(x,y)=(f1(x,y),f2(x,y))
def f(x,y):
    return np.array([x**2+2*x*y-1,(x*y)**2-y-3])
# Jacobien
def Jf(x,y):
    return np.array([np.array([2*x+2*y,2*x]),np.array([2*x*(y**2),2*(x**2)*y-1])])
u=np.zeros((6,2))
u[0]=np.array([3,-1])
for i in range(u.shape[0]-1):
    Jn=Jf(u[i,0],u[i,1])
    mfn=-f(u[i,0],u[i,1])
    z=np.linalg.solve(Jn,mfn)
    u[i+1]=u[i]+z
print(u)



